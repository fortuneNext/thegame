package movefinder;

import strategies.Strategy;

/**
 * Finds out which move to make next.
 */
public abstract class MoveFinder {
	Strategy strategy;

	public MoveFinder(Strategy strategy) {
		this.strategy = strategy;
	}

	public abstract void findNextMove();
}
