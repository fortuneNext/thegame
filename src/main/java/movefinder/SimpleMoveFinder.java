package movefinder;

import damage.damagecalculator.DamageCalculator;
import executor.Executor;
import game.Move;
import game.TheGame;
import strategies.Strategy;

/**
 * Finds the next move with best single rating.
 */
public class SimpleMoveFinder extends MoveFinder {

	public SimpleMoveFinder(Strategy strategy) {
		super(strategy);
	}

	@Override
	public void findNextMove() {
		final TheGame theGame = strategy.theGame;
		final Executor executor = strategy.executor;
		final DamageCalculator damageCalculator = strategy.damageCalculator;
		executor.nextDamage = Integer.MAX_VALUE;
		for (Integer card : theGame.activePlayer()) {
			for (int stack = 0; stack < theGame.topCards.length; stack++) {
				if (!theGame.fitsStack(card, stack)) {
					continue;
				}
				int damage;
				if (stack == 0 || stack == 1) {
					damage = damageCalculator.damage(theGame.topCards[stack], card, false);
				} else {
					damage = damageCalculator.damage(theGame.topCards[stack], card, true);
				}

				if (damage < executor.nextDamage) {
					executor.nextDamage = damage;
					executor.nextMove = new Move(card, stack);
				}
			}
		}
	}
}
