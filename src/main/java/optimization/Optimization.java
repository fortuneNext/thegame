package optimization;

import damage.damagecalculator.BucketDamage;
import executor.MinGreedyPlusX;
import game.TheGame;
import movefinder.SimpleMoveFinder;
import org.apache.commons.math3.distribution.BinomialDistribution;
import strategies.Strategy;
import turnmaker.ExtendedJumpTrick;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Tests parameter sets for a strategy.
 */
public class Optimization {
	HashMap<Long, Double> visited = new HashMap<>();
	List<ParamSet> localOptima = new LinkedList<>();
	final int maxSimSize = 1000000;

	public double parameterTest(ParamSet params) {
		final long paramsHash = params.hash();
		if (visited.containsKey(paramsHash)) {
			return visited.get(paramsHash);
		}
		return freshParameterTest(params, 2000);
	}

	public double freshParameterTest(ParamSet params, int simSize) {
		final AtomicInteger progress = new AtomicInteger(0);
		final double res = IntStream.range(0, simSize).parallel().filter((i) -> {
			System.out.print(progress.incrementAndGet() + " / " + simSize + "\r");
			final TheGame theGame = new TheGame(2);
			final Strategy newStrategy = new Strategy(theGame);
			newStrategy.damageCalculator = new BucketDamage(theGame, params.w0, params.w1, params.w2);
			newStrategy.executor = new MinGreedyPlusX(newStrategy, params.threshold);
			newStrategy.moveFinder = new SimpleMoveFinder(newStrategy);
			newStrategy.turnMaker = new ExtendedJumpTrick(newStrategy);
			return newStrategy.execute();
		}).count() / (double) simSize;
		System.out.println(params.toString() + " - " + res);
		visited.put(params.hash(), res);
		return res;
	}

	public ParamSet optimizeLocal(ParamSet params) {
		final double startVal = parameterTest(params);

		ParamSet[] compSets = new ParamSet[]{
				new ParamSet(params.w0 + 1, params.w1, params.w2, params.threshold),
				new ParamSet(params.w0 - 1, params.w1, params.w2, params.threshold),
				new ParamSet(params.w0, params.w1 + 1, params.w2, params.threshold),
				new ParamSet(params.w0, params.w1 - 1, params.w2, params.threshold),
				new ParamSet(params.w0, params.w1, params.w2 + 1, params.threshold),
				new ParamSet(params.w0, params.w1, params.w2 - 1, params.threshold),
				new ParamSet(params.w0, params.w1, params.w2, params.threshold + 1),
				new ParamSet(params.w0, params.w1, params.w2, params.threshold - 1),
				params
		};

		for (int i = 0; i < compSets.length - 1; i++) {
			if (!compSets[i].valid()) {
				continue;
			}
			final double compVal = parameterTest(compSets[i]);
			if (compVal > startVal) {
				compSets[i] = optimizeLocal(compSets[i]);
			}
		}

		return Arrays.stream(compSets).max(Comparator.comparingDouble(this::parameterTest)).orElse(params);
	}

	public void search() {
		ParamSet set = new ParamSet(5, 4, 2, 5);
		while (true) {
			localOptima.add(optimizeLocal(set));
			System.out.println(localOptima);
			final ParamSet best = bestSet();
			System.out.println("Best set: " + best + " (" + parameterTest(best) + ")");

			set = randomSet();
			while (2 * parameterTest(set) < bestVal()) {
				set = randomSet();
			}
		}
	}

	public void searchAll() {
		ParamSet best = new ParamSet(5, 3, 2, 5);
		double bestVal = freshParameterTest(best, maxSimSize);
		for (int w0 = 1; w0 <= 16; w0++) {
			for (int w1 = 1; w1 <= w0; w1++) {
				for (int w2 = 1; w2 <= w1; w2++) {
					for (int t = (int) (w0 * 0.8); t <= w0 * 1.2; t++) {
						final ParamSet testSet = new ParamSet(w0, w1, w2, t);
						double val = parameterTest(testSet);
						final double difference = Math.abs(val - bestVal);
						if (val > bestVal || difference < 0.03) {
							System.out.println("New best set candidate: " + testSet);
							val = freshParameterTest(testSet, findN(Math.abs(val - bestVal), val));
							if (val > bestVal) {
								bestVal = val;
								best = testSet;
								System.out.println("New best set: " + best + " (" + bestVal + ")");
							}
						}
					}
				}
			}
		}
		System.out.println("Best: " + best + " (" + bestVal + ")");
	}

	public ParamSet randomSet() {
		final int w0 = (int) (Math.random() * 10) + 1;
		final int w1 = (int) (Math.random() * w0) + 1;
		final int w2 = (int) (Math.random() * w1) + 1;
//		final int threshold = (int) (0.5 + Math.random() * (w0));
		final int threshold = (w0);

		return new ParamSet(w0, w1, w2, threshold);
	}

	public ParamSet bestSet() {
		return localOptima.stream().max(Comparator.comparingDouble(this::parameterTest)).get();
	}

	public double bestVal() {
		return parameterTest(bestSet());
	}

	private int findN(double difference, double p) {
		long timing = System.currentTimeMillis();
		int n = 1000;
		BinomialDistribution dist = new BinomialDistribution(n, p);
		while (n < maxSimSize && dist.cumulativeProbability(
				(int) (n * (p - difference / 2)),
				(int) (n * (p + difference / 2))
		) < 0.90) {
			n *= 2;
			dist = new BinomialDistribution(n, p);
		}

		System.out.println("Required N: " + n + " took " + (System.currentTimeMillis() - timing));

		return n;
	}
}
