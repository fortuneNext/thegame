package optimization;

/**
 * A utility parameter set for BucketDamage.
 */
public class ParamSet {
	int w0, w1, w2, threshold;

	public ParamSet(int w0, int w1, int w2, int threshold) {
		this.w0 = w0;
		this.w1 = w1;
		this.w2 = w2;
		this.threshold = threshold;
	}

	public ParamSet(ParamSet paramSet, int threshold) {
		this(paramSet.w0, paramSet.w1, paramSet.w2, threshold);
	}

	private static long pair(long a, long b) {
		return (long) (0.5 * (a + b) * (a + b + 1) + b);
	}

	public long hash() {
		return pair(w0, pair(w1, pair(w2, threshold)));
	}

	public boolean valid() {
		return w0 >= 0 && w1 >= 0 && w2 >= 0;
	}

	@Override
	public String toString() {
		return "ParamSet: " + w0 + " " + +w1 + " " + +w2 + " " + threshold;
	}
}
