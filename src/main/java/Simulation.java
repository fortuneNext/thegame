import damage.damagecalculator.AdvancedDamage;
import executor.MinGreedyPlusX;
import game.TheGame;
import movefinder.SimpleMoveFinder;
import optimization.Optimization;
import strategies.Strategy;
import turnmaker.ExtendedJumpTrick;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Simulation {
	static AtomicInteger progress = new AtomicInteger(0);
	static int simSize = 100000;

	public static void main(String[] args) {

//		optimize();

		final boolean load = false;
		final int numPlayers = 2;

		long timing = System.currentTimeMillis();

		final int count = load ? simLoadGames() :
				(int) IntStream.range(0, simSize).parallel().filter((i) -> {
					System.out.print(progress.incrementAndGet() + " / " + simSize + "\r");
					return createStrategy(numPlayers).execute();
				}).count();

		long timingEnd = System.currentTimeMillis() - timing;
		System.out.println("Overall took " + timingEnd + "ms | " + timingEnd / (double) simSize +
				"ms per simulation | " + 1000 * simSize / timingEnd + "sim/s");
		System.out.println("Won: " + count / (double) simSize + " (" + count + ")");
	}

	public static int simLoadGames() {
		final Stream<File> files = Arrays.stream(Objects.requireNonNull(new File("games").listFiles()))
				.filter(File::isFile);
		simSize = (int) files.count();
		System.out.println(simSize + " simulations loaded");
		return (int) files.filter(File::isFile).map(file -> {
			try {
				return new TheGame(file);
			} catch (Exception e) {
				System.out.println("Failed reading file" + file.toString());
				e.printStackTrace();
			}
			return null;
		}).filter(Objects::nonNull).filter((game) -> {
			System.out.print(progress.incrementAndGet() + " / " + simSize + "\r");
			return createStrategy(game).execute();
		}).count();
	}

	public static void saveGame(Strategy sim) {
		try {
			FileWriter writer = new FileWriter("games/solutions/" + sim.theGame.name);
			writer.write(sim.getLog());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Strategy createStrategy(int numPlayers) {
		return createStrategy(new TheGame(numPlayers));
	}

	public static Strategy createStrategy(TheGame theGame) {
		final Strategy newStrategy = new Strategy(theGame);
//		newStrategy.damageCalculator = new BucketDamage(theGame, 5, 3, 2);
		newStrategy.damageCalculator = new AdvancedDamage(theGame);
		newStrategy.executor = new MinGreedyPlusX(newStrategy, 1);
		newStrategy.moveFinder = new SimpleMoveFinder(newStrategy);
		newStrategy.turnMaker = new ExtendedJumpTrick(newStrategy);
		return newStrategy;
	}

	public static void optimize() {
		Optimization opt = new Optimization();
		opt.searchAll();
		System.exit(0);
	}
}
