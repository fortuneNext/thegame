package strategies;

import damage.damagecalculator.DamageCalculator;
import executor.Executor;
import game.Move;
import game.TheGame;
import movefinder.MoveFinder;
import turnmaker.TurnMaker;

/**
 * An instance of a concrete game with different components helping to solve that game, waiting to be executed.
 */
public class Strategy {
	public Executor executor;
	public MoveFinder moveFinder;
	public DamageCalculator damageCalculator;
	public TheGame theGame;
	public TurnMaker turnMaker;

	public Strategy(TheGame theGame) {
		this.theGame = theGame;
	}

	public boolean playCard(Move move) {
		return theGame.playCard(move);
	}

	public boolean execute() {
		return executor.execute();
	}

	public int requiredTurns() {
		return theGame.stack.isEmpty() ? (theGame.activePlayer().isEmpty() ? 0 : 1) : 2;
	}

	public String getLog() {
		return theGame.log;
	}
}
