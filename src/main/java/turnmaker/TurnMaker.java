package turnmaker;

import game.Move;
import game.Turn;
import strategies.Strategy;

/**
 * Converts moves to turns with room for optimization of said moves.
 */
public class TurnMaker {
	Strategy strategy;

	public TurnMaker(Strategy strategy) {
		this.strategy = strategy;
	}

	static Integer jumpFromCard(Move move) {
		final int card = move.card, stack = move.stack;
		if (stack == 0 || stack == 1) {
			return card + 10;
		} else if (stack == 2 || stack == 3) {
			return card - 10;
		} else {
			return card;
		}
	}

	static Integer jumpToCard(Move move) {
		final int card = move.card, stack = move.stack;
		if (stack == 0 || stack == 1) {
			return card - 10;
		} else if (stack == 2 || stack == 3) {
			return card + 10;
		} else {
			return card;
		}
	}

	public Turn makeTurn(Move move) {
		return new Turn(move);
	}
}
