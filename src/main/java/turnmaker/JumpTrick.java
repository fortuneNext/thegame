package turnmaker;

import game.Move;
import game.Turn;
import strategies.Strategy;

/**
 * Always puts any jump card before each card, e.g. when playing a 10 on a rising stack, could put 30 and 20 before.
 */
public class JumpTrick extends TurnMaker {
	public JumpTrick(Strategy strategy) {
		super(strategy);
	}

	@Override
	public Turn makeTurn(Move move) {
		final Turn res = new Turn();
		addBetterMovesToTurn(move, res);
		return res;
	}

	void addBetterMovesToTurn(Move move, Turn turn) {
		final Integer betterCard = jumpFromCard(move);
		if (strategy.theGame.activePlayer().contains(betterCard)) {
			addBetterMovesToTurn(new Move(betterCard, move.stack), turn);
		}
		turn.add(move);
	}
}
