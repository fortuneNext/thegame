package turnmaker;

import game.Move;
import game.TheGame;
import game.Turn;
import strategies.Strategy;

/**
 * Always puts any jump card and any card in between before each card,
 * e.g. when playing a 10 on a rising stack, could put 30 and 20 and any card in between 10 and 30 before.
 */
public class ExtendedJumpTrick extends JumpTrick {
	public ExtendedJumpTrick(Strategy strategy) {
		super(strategy);
	}

	// TODO kommt kacke bei raus, weil der eigentliche move nicht gemacht werden kann,
	// weil die jumpToCard dann auf der Hand ist
	@Override
	void addBetterMovesToTurn(Move move, Turn turn) {
		final int stack = move.stack;
		final TheGame theGame = strategy.theGame;
		final Integer betterCard = jumpFromCard(move);
		if (theGame.activePlayer().contains(betterCard)) {
			if (stack == 0 || stack == 1) {
				for (int i = move.card + 1; i < betterCard; i++) {
					final Move possibleMove = new Move(i, stack);
					if (theGame.activePlayer().contains(i)
							&& !theGame.activePlayer().contains(jumpToCard(possibleMove))) {
						addBetterMovesToTurn(possibleMove, turn);
					}
				}
				addBetterMovesToTurn(new Move(betterCard, stack), turn);
			} else if (stack == 2 || stack == 3) {
				for (int i = move.card - 1; i > betterCard; i--) {
					final Move possibleMove = new Move(i, stack);
					if (theGame.activePlayer().contains(i)
							&& !theGame.activePlayer().contains(jumpToCard(possibleMove))) {
						addBetterMovesToTurn(possibleMove, turn);
					}
				}
				addBetterMovesToTurn(new Move(betterCard, stack), turn);
			}
		}
		turn.add(move);
	}
}
