package damage.damagecalculator;

import game.TheGame;

import java.util.LinkedList;

/**
 * Rates a move by considering which cards can be played on how many stacks after that move and weights those numbers.
 */
public class BucketDamage implements DamageCalculator {
	TheGame theGame;
	final int w0, w1, w2;

	public BucketDamage(TheGame theGame, int w0, int w1, int w2) {
		this.theGame = theGame;
		this.w0 = w0;
		this.w1 = w1;
		this.w2 = w2;
	}

	@Override
	public int damage(int oldValue, int newValue, boolean backwards) {
		final boolean jump = oldValue > newValue ^ backwards;
		//noinspection unchecked
		LinkedList<Integer>[] buckets = new LinkedList[5];
		for (int i = 0; i < buckets.length; i++) {
			buckets[i] = new LinkedList<>();
		}
		for (int i = Math.min(oldValue, newValue); i <= Math.max(oldValue, newValue); i++) {
			if (!theGame.playedCards[i]) {
				buckets[theGame.fittingStacks(i) - (jump ? 0 : 1)].add(i);
			}
		}

		final int res = w0 * buckets[0].size()
				+ w1 * buckets[1].size()
				+ w2 * buckets[2].size()
				+ buckets[3].size();

		return (jump) ? (res - 1) * -1 : res;
	}
}
