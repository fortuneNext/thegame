package damage.damagecalculator;

/**
 * Rates a move.
 */
public interface DamageCalculator {
	int damage(int oldValue, int newValue, boolean backwards);
}
