package damage.damagecalculator;

import game.TheGame;

/**
 * Evaluate a move by the gap covered, ignoring already played cards in between.
 */
public class AdvancedDamage implements DamageCalculator {
	private final TheGame theGame;

	public AdvancedDamage(TheGame theGame) {
		this.theGame = theGame;
	}

	@Override
	public int damage(int oldValue, int newValue, boolean backwards) {
		return (oldValue < newValue ^ backwards) ?
				theGame.unplayedNumbersBetween(oldValue, newValue) :
				(theGame.unplayedNumbersBetween(oldValue, newValue) - 1) * -1;
	}
}
