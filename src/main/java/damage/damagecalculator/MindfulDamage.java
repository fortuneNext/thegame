package damage.damagecalculator;

import game.TheGame;

/**
 * Considers the hand cards of the next player to calculate a rating by using his next best move.
 */
public class MindfulDamage implements DamageCalculator {
	TheGame theGame;
	boolean[] playedCards;
	DamageCalculator advancedDamage;

	public MindfulDamage(TheGame theGame) {
		this.theGame = theGame;
		this.playedCards = theGame.playedCards;
		advancedDamage = new AdvancedDamage(theGame);
	}

	@Override
	public int damage(int oldValue, int newValue, boolean backwards) {
		int res = advancedDamage.damage(oldValue, newValue, backwards);

		int wouldDamageMin = 1000;
		for (Integer card : theGame.nextPlayer()) {
			if (!TheGame.fitsTopNumber(card, oldValue, backwards)) {
				continue;
			}
			int wouldDamage = advancedDamage.damage(oldValue, card, backwards);

			if (wouldDamage < wouldDamageMin) {
				wouldDamageMin = wouldDamage;
			}
		}
		if (wouldDamageMin < res) {
			res += res - wouldDamageMin;
		}

		return res;
	}
}
