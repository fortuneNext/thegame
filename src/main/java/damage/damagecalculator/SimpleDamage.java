package damage.damagecalculator;

/**
 * Evaluate a move by the gap covered.
 */
public class SimpleDamage implements DamageCalculator {
	@Override
	public int damage(int oldValue, int newValue, boolean backwards) {
		int res = (newValue - oldValue);
		return backwards ? -1 * res : res;
	}
}
