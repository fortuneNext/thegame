package damage.damagecalculator;

/**
 * Evaluate a move by the gap covered, but rates jumps as really bad (for testing purposes).
 */
public class NoJumps implements DamageCalculator {
	private DamageCalculator baseCalculator;

	@Override
	public int damage(int oldValue, int newValue, boolean backwards) {
		int res = (newValue - oldValue);
		res = backwards ? -1 * res : res;
		return res < 0 ? 500 : res;
	}
}
