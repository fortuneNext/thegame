package damage.damagecalculator;

import game.Player;
import game.TheGame;

/**
 * Considers the hand cards of all players to calculate a rating by using his next best move recursively.
 */
public class MindfulDamageRec extends MindfulDamage {

	public MindfulDamageRec(TheGame theGame) {
		super(theGame);
	}

	int damage(int oldValue, int newValue, boolean backwards, Player nextPlayer) {
		final int initialRes = advancedDamage.damage(oldValue, newValue, backwards);
		if (nextPlayer == theGame.activePlayer()) {
			return initialRes;
		}

		int res = initialRes;
		res += nextPlayer.stream().filter(card ->
				TheGame.fitsTopNumber(card, oldValue, backwards)
		).mapToInt(card ->
				damage(oldValue, card, backwards, theGame.nextPlayer(nextPlayer))
		).filter(wouldDamage -> wouldDamage < initialRes).map(wouldDamage -> initialRes - wouldDamage).sum();

		return res;
	}

	@Override
	public int damage(int oldValue, int newValue, boolean backwards) {
		return damage(oldValue, newValue, backwards, theGame.nextPlayer());
	}
}
