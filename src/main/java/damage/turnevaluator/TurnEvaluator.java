package damage.turnevaluator;

import game.Turn;

/**
 * Evaluates whole turns in opposition to single moves.
 */
public interface TurnEvaluator {
	int damage(Turn turn);
}
