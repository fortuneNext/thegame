package damage.turnevaluator;

import damage.damagecalculator.SimpleDamage;
import game.TheGame;
import game.Turn;

/**
 * Adds up the simple damage for each move in the turn.
 */
public class SimpleTurnDamage implements TurnEvaluator {
	SimpleDamage simpleDamage = new SimpleDamage();
	TheGame theGame;

	SimpleTurnDamage(TheGame theGame) {
		this.theGame = theGame;
	}

	@Override
	public int damage(Turn turn) {
		return simpleDamage.damage(theGame.topCards[0],
				turn.stream().filter(move -> move.stack == 0).mapToInt(move -> move.card).max().orElse(theGame.topCards[0]), false)
				+ simpleDamage.damage(theGame.topCards[1],
				turn.stream().filter(move -> move.stack == 1).mapToInt(move -> move.card).max().orElse(theGame.topCards[1]), false)
				+ simpleDamage.damage(theGame.topCards[2],
				turn.stream().filter(move -> move.stack == 2).mapToInt(move -> move.card).min().orElse(theGame.topCards[2]), true)
				+ simpleDamage.damage(theGame.topCards[3],
				turn.stream().filter(move -> move.stack == 3).mapToInt(move -> move.card).min().orElse(theGame.topCards[3]), true);
	}
}
