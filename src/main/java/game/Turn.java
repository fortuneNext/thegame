package game;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * A turn that consists of multiple moves.
 */
public class Turn extends LinkedList<Move> {
	public Turn(Move... moves) {
		addAll(Arrays.asList(moves));
	}
}
