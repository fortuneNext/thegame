package game;

/**
 * A single card put to a single stack.
 */
public class Move {
	public final Integer card;
	public final int stack;

	public Move(Integer card, int stack) {
		this.card = card;
		this.stack = stack;
	}

	@Override
	public String toString() {
		return card + "->" + stack;
	}
}
