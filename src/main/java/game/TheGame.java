package game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Stack;
import java.util.stream.IntStream;

/**
 * Representation of the state of the game and its rules and possible moves.
 */
public class TheGame {
	public final boolean[] playedCards = new boolean[101];
	public final Stack<Integer> stack = new Stack<>();
	public final int[] topCards = new int[]{0, 0, 100, 100};
	public final Player[] players;
	public final int handSize;
	private int activePlayer;
	private boolean playedCard = false;
	public String log;
	public String name;
	final boolean doLog = false;

	public TheGame(File file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		String stack = br.readLine(),
				player0 = br.readLine(),
				player1 = br.readLine();

		players = new Player[2];
		handSize = 7;
		players[0] = new Player();
		players[1] = new Player();

		stack = stack.substring(stack.indexOf(" ") + 1);
		player0 = player0.substring(player0.indexOf(":") + 2);
		player1 = player1.substring(player1.indexOf(":") + 2);

		for (String card : stack.split(",")) {
			this.stack.push(Integer.valueOf(card));
		}
		for (String card : player0.split(",")) {
			players[0].add(Integer.valueOf(card));
		}
		for (String card : player1.split(",")) {
			players[1].add(Integer.valueOf(card));
		}
		playedCards[0] = true;
		playedCards[1] = true;
		playedCards[100] = true;

		name = file.getName();
		log = toString() + System.lineSeparator();
	}

	public TheGame(int numPlayers) {
		this(numPlayers, Math.max(6, 9 - numPlayers));
	}

	public TheGame(int numPlayers, int handSize) {
		if (numPlayers > 16) {
			throw new IllegalArgumentException("Too many players");
		}
		this.handSize = handSize;
		playedCards[0] = true;
		playedCards[1] = true;
		playedCards[100] = true;
		for (int i = 2; i <= 99; i++) {
			stack.push(i);
		}
		Collections.shuffle(stack);
		players = new Player[numPlayers];
		for (int p = 0; p < numPlayers; p++) {
			final Player newPlayer = new Player();
			for (int i = 0; i < handSize; i++) {
				newPlayer.add(stack.pop());
			}
			players[p] = newPlayer;
		}
		log = toString() + System.lineSeparator();
	}

	public TheGame(TheGame theGame) {
		System.arraycopy(theGame.playedCards, 0, playedCards, 0, theGame.playedCards.length);
		stack.addAll(theGame.stack);
		System.out.println(theGame.stack);
		System.out.println(stack);
		handSize = theGame.handSize;
		activePlayer = theGame.activePlayer;
		System.arraycopy(theGame.topCards, 0, topCards, 0, 4);
		playedCard = theGame.playedCard;
		players = new Player[theGame.players.length];
		for (int i = 0; i < theGame.players.length; i++) {
			final Player newPlayer = new Player();
			newPlayer.addAll(theGame.players[i]);
			players[i] = newPlayer;
		}
		log = theGame.log;
	}

	public static boolean fitsTopNumber(Integer card, int topCard, boolean backwards) {
		return backwards ? card < topCard || card == topCard + 10 : card > topCard || card == topCard - 10;
	}

	public boolean playCard(Move move) {
		final Integer card = move.card;
		final int stack = move.stack;
		if (!activePlayer().contains(card) || activePlayer() != activePlayer() || !fitsStack(card, stack)) {
			return false;
		}

		activePlayer().remove(card);
		playedCards[card] = true;
		topCards[stack] = card;
		playedCard = true;

		if (doLog) {
			log += "----- Player " + activePlayer + ": " + card + " to " + stack + System.lineSeparator() + toString() + System.lineSeparator();
		}

		return true;
	}

	public Player activePlayer() {
		return players[activePlayer];
	}

	public Player nextPlayer() {
		return players[(activePlayer + 1) % players.length];
	}

	public Player nextPlayer(Player currentPlayer) {
		return players[(Arrays.asList(players).indexOf(currentPlayer) + 1) % players.length];
	}

	public String toString() {
		return "Stack Size: " + stack.size() + System.lineSeparator()
				+ "topCards: " + topCards[0] + " " + topCards[1] + " " + topCards[2] + " " + topCards[3] + System.lineSeparator()
				+ "Player Hands " + Arrays.toString(players) + System.lineSeparator();
	}

	public boolean endTurn() {
		if (!canFinishTurn()) {
			return false;
		}

		while (activePlayer().size() < handSize && !stack.isEmpty()) {
			activePlayer().add(stack.pop());
		}

		activePlayer = (activePlayer + 1) % players.length;
		playedCard = false;

		if (doLog) {
			log += "----- Player " + activePlayer + " ENDTURN" + System.lineSeparator() + toString() + System.lineSeparator();
		}

		return true;
	}

	public boolean isWon() {
		boolean res = true;
		for (Player player : players) {
			if (!player.isEmpty()) {
				res = false;
				break;
			}
		}

		return res && stack.isEmpty();
	}

	public boolean isLost() {
		return !(turnPossible() || canFinishTurn());
	}

	public boolean turnPossible() {
		if (activePlayer().isEmpty()) {
			return false;
		}
		for (Integer card : activePlayer()) {
			for (int stack = 0; stack < topCards.length; stack++) {
				if (fitsStack(card, stack)) {
					return true;
				}
			}
		}

		return false;
	}

	public boolean isFinished() {
		return isWon() || isLost();
	}

	public boolean canFinishTurn() {
		return stack.isEmpty()
				? playedCard || activePlayer().isEmpty()
				: (activePlayer().size() <= handSize - 2);
	}

	public boolean fitsStack(Integer card, int stack) {
		if (stack == 0 || stack == 1) {
			return fitsTopNumber(card, topCards[stack], false);
		} else if (stack == 2 || stack == 3) {
			return fitsTopNumber(card, topCards[stack], true);
		} else {
			return false;
		}
	}

	public int fittingStacks(Integer card) {
		int res = 0;
		for (int i = 0; i < topCards.length; i++) {
			if (fitsStack(card, i)) {
				res++;
			}
		}

		return res;
	}

	public int unplayedNumbersBetween(int from, int to) {
		return (int) IntStream.range(Math.min(from, to), Math.max(from, to) + 1).filter(i -> !playedCards[i]).count();
	}
}
