package executor;

import game.TheGame;
import strategies.Strategy;

/**
 * Tries to put as few cards as possible, each individually with the best scoring.
 */
public class MinGreedy extends Executor {
	public MinGreedy(Strategy strategy) {
		super(strategy);
	}

	@Override
	public boolean execute() {
		TheGame theGame = strategy.theGame;
		while (!theGame.isFinished()) {
			madeMoves = 0;
			while (madeMoves < strategy.requiredTurns()) {
				strategy.moveFinder.findNextMove();
				strategy.turnMaker.makeTurn(nextMove).forEach(move -> {
					strategy.playCard(move);
					madeMoves++;
				});
			}
			theGame.endTurn();
		}
		return theGame.isWon();
	}
}