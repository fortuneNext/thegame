package executor;

import game.TheGame;
import strategies.Strategy;

/**
 * Puts individual cards with best scoring until a certain threshold is broken.
 */
public class MinGreedyPlusX extends Executor {
	int threshold;

	public MinGreedyPlusX(Strategy strategy, int threshold) {
		super(strategy);
		this.threshold = threshold;
	}


	@Override
	public boolean execute() {
		TheGame theGame = strategy.theGame;
		while (!theGame.isFinished()) {
			strategy.moveFinder.findNextMove();
			madeMoves = 0;
			while (madeMoves < strategy.requiredTurns() || (nextDamage <= threshold && !theGame.activePlayer().isEmpty())) {
				strategy.turnMaker.makeTurn(nextMove).forEach(move -> {
					strategy.playCard(move);
					madeMoves++;
				});
				strategy.moveFinder.findNextMove();
			}
			theGame.endTurn();
		}
		return theGame.isWon();
	}
}
