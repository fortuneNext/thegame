package executor;

import game.Move;
import game.TheGame;
import strategies.Strategy;

import java.util.List;
import java.util.Random;

/**
 * Randomly plays cards until no move is possible (for testing purposes).
 */
public class RandomPlayAllHand extends Executor {
	Random random = new Random();

	public RandomPlayAllHand(Strategy strategy) {
		super(strategy);
	}

	@Override
	public boolean execute() {
		TheGame theGame = strategy.theGame;
		while (!theGame.isFinished()) {
			while (theGame.turnPossible()) {
				final List<Integer> hand = theGame.activePlayer();
				final Integer card = hand.get(random.nextInt(hand.size()));
				theGame.playCard(new Move(card, random.nextInt(4)));
			}
			theGame.endTurn();
		}
		return theGame.isWon();
	}
}