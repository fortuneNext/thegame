package executor;

import game.Move;
import strategies.Strategy;

/**
 * The component that actually plays cards.
 */
public abstract class Executor {
	public int madeMoves, nextDamage;
	public Move nextMove;
	Strategy strategy;

	public Executor(Strategy strategy) {
		this.strategy = strategy;
	}

	public abstract boolean execute();
}
